Test of memory load on reading processes.

Programmed in C++, uses SFML library for graphics.

Experiment flow:
1. Grid of 4 dots is displayed (high low/low load). Participant memorizes the dots' positions.
2. The grid disappears. A string of characters appears.
3. Participant presses button corresponding to the category the string of characters falls into.
4. The characters disappear and and empty grid appears. Participant clicks the grid in the positions of the 4 dots previously memorized.

Reaction times and accuracy for the text decision and dot position recreating are recorded.

Memory:
Low load - dots form a horizontal or verticle line
High load - dots are distributed randomly, with no dot directly beside or above another dot (diagonal neighbours are allowed).

Text (different experiments - compile with different -D flags):
-DVISUAL: text strings are visually identical or not (eg. AA -> yes, Aa -> no)
-DPHONO: text strings are not words, but either are pronounced identically to a real word or not (eg. brane -> yes, frane -> no)
-DSEMANTIC: text string refer to something man-made versus natural (eg. Hammer -> man-made, tree -> natural)
