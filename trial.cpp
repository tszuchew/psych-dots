#include "trial.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include "dotMatrix.h"
#include "respMatrix.h"

using namespace std;

Trial::Trial(Empty* e, LowLoad* l, HighLoad* h, RespMatrix* r, sf::RenderWindow* w, const unsigned int n):
	empty{e}, low{l}, high{h}, resp{r}, window{w}, numDots{n} {}

void Trial::displayGrid(DotMatrix* mat){
	mat->draw(window);
	window->display();
}

static sf::Event event;
static const sf::Keyboard::Key quitKey = sf::Keyboard::Key::Escape;

void Trial::setHighLoad(const bool b){highLoad = b;}

//handle input for closing the window
void Trial::pollWindow(sf::RenderWindow* window){
	while(window->pollEvent(event)){
		if (event.type == sf::Event::Closed){
			window->close();
			return;
		}
		if((event.type == sf::Event::KeyPressed) && (event.key.code == quitKey)){
			window->close();
			return;
		}
	}
}

//see what fraction of the dot placements were correct
float Trial::calculateAccuracy(DotMatrix& base){
	vector<sf::Vector2i> stim;
	vector<sf::Vector2i> response;
	float numCorrect = 0.0;
	for (unsigned int i = 0; i < numDots; ++i){
		stim.push_back(base.getDotPos(i));
		response.push_back(resp->getDotPos(i));
	}
	for (unsigned int i = 0; i < numDots; ++i){
		sf::Vector2i ref = response.at(i);
		for (unsigned int j = 0; j < numDots; ++j){
			if (stim.at(j).x == ref.x && stim.at(j).y == ref.y){
				++numCorrect;
				break;
			}
		}
	}	
	return numCorrect/static_cast<float>(numDots);
}

//user clicks in grid until enough dots have been placed to make resp->isFull() return true
//then calculate accuracy when they submit response by pressing continue
float Trial::memoryResponse(sf::Text* continueText, const sf::Keyboard::Key* continueKey){
	sf::FloatRect bounds = resp->getBounds();
	while(window->isOpen()){
		while(window->pollEvent(event)){
			if (resp->isFull()){
                            //enough responses given, so show continueText and allow them to press continueKey
				continueText->setColor(sf::Color::Black);
                                //on continue, calcualte accuracy
				if (event.type == sf::Event::KeyPressed && event.key.code == *continueKey){
					if (highLoad){
						return calculateAccuracy(*high);
					}else {
						return calculateAccuracy(*low);
					}
				}
			} else{	continueText->setColor(sf::Color::White);}
                        //poll for mouse clicks, get corresponding cell and place/remove dot
			if (event.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left)){
				sf::Vector2i mousePos = sf::Mouse::getPosition(*window);
				if (bounds.contains(mousePos.x, mousePos.y)){
					mousePos.x -= bounds.left;
					mousePos.y -= bounds.top;
					mousePos.x /= (bounds.width/numDots);
					mousePos.y /= (bounds.height/numDots);
					resp->placeDot(mousePos.x, mousePos.y, window);
				}
			}
			else if(event.type == sf::Event::KeyPressed && event.key.code == quitKey){
				window->close();
				return -1;
			}
		}
		window->draw(*continueText);
		window->display();
	}
	return -1;
}

//clock runs for duration many milliseconds, returns when time is up.
//Use for displaying stimuli for fixed period of time
void Trial::setTimer(const int duration){
	sf::Clock clock;
	while (window->isOpen()){
		if(clock.getElapsedTime().asMilliseconds() >= duration){
			return;
		}
		//allow window to be closed
		pollWindow(window);
	}
}

//wait for a valid response - one of sameKey or diffKey
//NOTE: only op1 and op2 are accepted as valid keys
char Trial::getResponse(const sf::Keyboard::Key* op1, const sf::Keyboard::Key* op2){
	while(window->isOpen()){
		while(window->pollEvent(event)){
			if (event.type == sf::Event::KeyPressed){
				if (event.key.code == *op1){
					return *op1;
				}else if(event.key.code == *op2){
					return *op2;
				}
			}
		}
	}
	//only reach here if we closed the window
	return ('x' - 'A');
}

void Trial::stimGrid(){
	if (highLoad){
		displayGrid(high);
	}else{
		displayGrid(low);
	}
}

void Trial::emptyGrid(){
	displayGrid(empty);
}

void Trial::respGrid(){
	displayGrid(resp);
}

void Trial::resetResp(){
	resp->reset();
}

