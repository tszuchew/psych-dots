#ifndef RESP_MATRIX_H
#define RESP_MATRIX_H

#include "dotMatrix.h"

class RespMatrix final: public DotMatrix{
	sf::FloatRect bounds;
	void placeDots() override;
	void updateToNewPosition() override;
	unsigned int dotsPlaced = 0;
	public:
	RespMatrix(const unsigned int s, const unsigned int n, const unsigned int xp=0, const unsigned int yp=0);
	bool isFull() const;
	sf::FloatRect& getBounds();
	void placeDot(const unsigned int x, const unsigned int y, sf::RenderWindow* window);
	void drawDots(sf::RenderWindow* window) override;
	void reset();
};


#endif
