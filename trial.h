#ifndef TRIAL_H
#define TRIAL_H
#include <SFML/Graphics.hpp>

class RespMatrix;
class DotMatrix;
class HighLoad;
class LowLoad;
class Empty;

class Trial{
	Empty* empty;
	LowLoad* low;
	HighLoad* high;
	RespMatrix* resp;
	sf::RenderWindow* window;
	const unsigned int numDots;
	bool highLoad;
	float calculateAccuracy(DotMatrix& base);
	void displayGrid(DotMatrix* mat);
	void pollWindow(sf::RenderWindow* win);
	public:
	Trial(Empty* e, LowLoad* l, HighLoad* h, RespMatrix* r, sf::RenderWindow* w, const unsigned int n);
	void stimGrid();
	void emptyGrid();
	void respGrid();
	void setHighLoad(const bool b);
	char getResponse(const sf::Keyboard::Key* op1, const sf::Keyboard::Key* op2);
	void setTimer(const int duration);
	float memoryResponse(sf::Text* continueText, const sf::Keyboard::Key* continueKey);
	void resetResp();

};

#endif
