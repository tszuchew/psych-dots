CXX = g++-5
#EXPFLAG = -DVISUAL
#EXPFLAG = -DSEMANTIC
EXPFLAG = -DPHONO
CXXFLAGS = -std=c++14 -Wall -MMD ${EXPFLAG}
EXEC = run
LDFLAGS = -lsfml-graphics -lsfml-window -lsfml-system


DEPENDS = ${OBJECTS:.o=.d}
OBJECTS = main.o dotMatrix.o respMatrix.o trial.o

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} ${LDFLAGS} -o ${EXEC}

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
