#ifndef DOTMATRIX_H
#define DOTMATRIX_H
#include<SFML/Graphics.hpp>

class Dot;

struct DotMatrix{
	const unsigned int size; //height and width of whole grid
	unsigned int x = 0; //x position of grid
	unsigned int y = 0; //y position of grid
	const unsigned int dist; //size of a single cell (size/numCells)
	Dot* dots; //the dots to draw in cells
	sf::RectangleShape outline; //outline of grid
	void drawGrid(sf::RenderWindow* window) const;
	virtual void placeDots() = 0;
	virtual void updateToNewPosition();
	public:
	const unsigned int numCells; //number of cells in each row/col (numcells = 4 --> grid is 4x4)
	DotMatrix(const unsigned int s, const unsigned int n, const unsigned int xpos, const unsigned int ypos);
	virtual ~DotMatrix();
	void draw(sf::RenderWindow* window);
	void setPosition(const unsigned int xp, const unsigned int yp); //x = xp, y = yp
	unsigned int getX() const; //returns x
	unsigned int getY() const; //returns y
	unsigned int getSize() const; //returns size
	sf::RectangleShape* getOutline();
	void setDotPos(const int i, const int xp, const int yp);
	sf::Vector2i getDotPos(const unsigned int i);
	virtual void drawDots(sf::RenderWindow* window);
};

class LowLoad final: public DotMatrix{
	void placeDots() override; //put dots in a single vertical or horizontal line
	public:
	LowLoad(const unsigned int s, const unsigned int n, const unsigned int xp=0, const unsigned int yp=0):
	       	DotMatrix{s,n,xp,yp} {}
};

class HighLoad final: public DotMatrix{
	//checkNeighbours returns true if a Dot at dots[j], j < i, is a vertical OR horizontal neighbour of dots[i].
	// ie. diagonal will return false
	bool checkNeighbours(const unsigned int i, const int xCell, const int yCell);
	void placeDots() override; //scatter dots, do not allow any horizontal or vertical neighbours (checkNeighbours must return false for each Dot)
	public:
	HighLoad(const unsigned int s, const unsigned int n, const unsigned int xp=0, const unsigned int yp=0):
	       	DotMatrix{s,n,xp,yp} {}
};

class Empty final: public DotMatrix{
	void placeDots() override; //does nothing
	public:
	Empty(const unsigned int s, const unsigned int n, const unsigned int xp=0, const unsigned int yp=0):
	       	DotMatrix{s,n,xp,yp} {}
	void drawDots(sf::RenderWindow* window) override; //does nothing - don't draw any dots
};

#endif
