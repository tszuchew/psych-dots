#include "respMatrix.h"
#include "dot.h"

RespMatrix::RespMatrix(const unsigned int s, const unsigned int n, const unsigned int xp, const unsigned int yp):
	DotMatrix{s,n,xp,yp} {
		bounds = getOutline()->getGlobalBounds();
	}

void RespMatrix::placeDots(){}

void RespMatrix::updateToNewPosition(){
	bounds = getOutline()->getGlobalBounds();
}

bool RespMatrix::isFull() const {return dotsPlaced == numCells;}

sf::FloatRect& RespMatrix::getBounds(){
	return bounds;
}

void RespMatrix::drawDots(sf::RenderWindow* window) {
}

void RespMatrix::placeDot(const unsigned int xCell, const unsigned int yCell, sf::RenderWindow* window){
	if (xCell >= numCells || yCell >= numCells){
		return;
	}
	unsigned int i = 0;
        //check if clicked in place that already has a dot
	while (i < dotsPlaced){
		if (dots[i].xCell == xCell && dots[i].yCell == yCell){
			break;
		}
		++i;
	}
        //clicked on new spot. update dot info, draw new dot.
	if (i == dotsPlaced){
		if (dotsPlaced < numCells){
			++dotsPlaced;
			setDotPos(i, xCell, yCell);
			window->draw(dots[i].dot);
		} else {
			return;
		}
	} else{
                //clicked on spot with dot in it already, so remove dot and allow user to place somewhere else.
		if (dotsPlaced > 0){
                        //draw white dot to cover up existing black dot,
                        //decrement dotsPlaced to allow placing another dot
			dots[i].dot.setFillColor(sf::Color::White);
			window->draw(dots[i].dot);
			dots[i].dot.setFillColor(sf::Color::Black);
			--dotsPlaced;
			unsigned int j = i;
                        //shift other dots down
			while (j < dotsPlaced){
				setDotPos(j, dots[j+1].xCell, dots[j+1].yCell);
				++j;
			}
		}
	}

}


void RespMatrix::reset(){
	dotsPlaced = 0;
}
