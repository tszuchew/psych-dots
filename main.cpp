#include <SFML/Graphics.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
#include "dotMatrix.h"
#include "respMatrix.h"
#include "trial.h"

using namespace std;


const sf::VideoMode screen = sf::VideoMode::getDesktopMode();
const int screenWidth = screen.width;
const int screenHeight = screen.height;
const int gridSize = screenHeight*0.7;
const sf::Vector2i gridPos = sf::Vector2i((screenWidth-gridSize)/2, (screenHeight-gridSize)/2);
const int numDots = 4;

//TIMING & RESPONSES
const unsigned int practiceTrials = 16; //number of trials in practice block
#if defined PHONO
const unsigned int experTrials = 100;
const unsigned int trialsPerBlock = 50; //number of trials in experimental blocks
#else
const unsigned int trialsPerBlock = 64; //number of trials in experimental blocks
const unsigned int experTrials = 160;
#endif
const unsigned int totalTrials = experTrials+practiceTrials; //total number of experimental +practice trials
const unsigned int typesOfTrial = 4; //lowA, lowB, highA, highB
const unsigned int numEach = experTrials/typesOfTrial; //number of words to have in each file (High/Low A, High/Low B)
const unsigned int pracEach = practiceTrials/typesOfTrial; //number of words to have in each file (High/Low A, High/Low B)
unsigned int trialsRemaining[4] = {numEach, numEach, numEach, numEach};
unsigned int pracRemaining[4] = {pracEach, pracEach, pracEach, pracEach};
const unsigned int numBlocks = ceil(experTrials/trialsPerBlock); //number of experimental blocks
const unsigned int displayGridTime = 750;
const unsigned int maskGridTime = 250;
const unsigned int fixationTime = 500;
const unsigned int interTrialTime = 1500;
const char keyChars[2] = {'F','J'};
const sf::Keyboard::Key keys[2] = {sf::Keyboard::Key::F, sf::Keyboard::Key::J};
char yesKeyChar;
char noKeyChar;
sf::Keyboard::Key yesKey;
sf::Keyboard::Key noKey;
const sf::Keyboard::Key quitKey = sf::Keyboard::Escape;
const sf::Keyboard::Key continueKey = sf::Keyboard::Space;
bool inPractice = true;


sf::RenderWindow window; //the window to which everything will be rendered
sf::RenderTexture bgTex; //pure white texture for background
sf::Sprite bg; //the background that will be rendered
sf::Font font; //font for the text and fixation cross
const unsigned int fixSize = 48; //font size for fixation cross
const unsigned int fontSize = 64; //font size for stimuli
sf::Text fixationCross("+", font, fixSize);
sf::Text stimText("", font, fontSize); //stimulus for reading task
sf::Text continueText("Press Spacebar to continue", font, fontSize); //shown at top of memory response grid
sf::Text breakText("BREAK\nPress Spacebar to continue", font, fontSize/2); //shown between blocks
sf::Text introText("", font, fontSize/2); //instructions shown at the beginning of the experiment
sf::Text endText("Experiment over!\nThank you for participating\n\nSee the experimenter for information about this study", font, fontSize/2);//shown at end of final block
sf::Text leftReminder("", font, fontSize/2);
sf::Text rightReminder("", font, fontSize/2);
string leftRemind;
string rightRemind;
string instr;
string dummyText=""; //empty text
/*******************************************
//cat A -> YesKey
//cat B -> NoKey
//_1 -> Low mem load (in counterbalance 0)
//_2 -> High mem load (in counterbalance 0)
********************************************/
//VISUAL: A -> same, B -> diff
//SEMANTIC: A-> natural, B -> man-made
//PHONO: A-> pseudohomophone, B -> control

string fileNames[4] = {"A_1.txt", "B_1.txt", "A_2.txt", "B_2.txt"};
std::ifstream fileStreams[4];

//clear the screen by redrawing the white background
void clearScreen(){
	window.clear();
	window.draw(bg);
	window.display();
}

//left- or right-align text with offset from screen border
void align(sf::Text* t, const bool right, const float height = screenHeight/8, const int offset = 30){
    sf::FloatRect b = t->getLocalBounds();
    if (right){
        t->setPosition(screenWidth-(b.width/2)-offset, height);
    }else{
        t->setPosition(b.width/2+offset, height);
    }
}

//make font black, put origin at text's center so it's easier to position
void initText(sf::Text* t){
	t->setColor(sf::Color::Black);
	sf::FloatRect b = t->getLocalBounds();
	t->setOrigin(b.left + (b.width/2), b.top + (b.height/2));
}

//change the string of text t, recalc origin
void setText(sf::Text* t, string s){
	t->setString(s);
	initText(t);
}


//clear screen and draw t, wait for participant to press continueKey then clear again
void textScreen(sf::Text* t, Trial* trial, string& s, const sf::Keyboard::Key* contKey = &continueKey){
	clearScreen();
	t->setPosition(screenWidth/2, screenHeight/2);
	if (s != ""){
		setText(t, s);
	}
	window.draw(*t);
	window.display();
	trial->getResponse(contKey, contKey);
	clearScreen();
}

void textFromFile(string& s, ifstream& in){
	if (!(in >> s)){cerr << "ran out of input stimuli" << endl; exit(1);}
}


void doTrial(Trial t, ofstream& out){
	clearScreen();
	window.setMouseCursorVisible(false);
	//pick what type of trial this will be
	unsigned int* remaining = (inPractice)? &(pracRemaining[0]) : &(trialsRemaining[0]);	
	int trialType = rand()%4;
	while(remaining[trialType] <= 0){
		trialType = rand()%4;
	}
	--remaining[trialType];
	bool highLoad = trialType > 1;
	bool stimFalse = trialType%2;
        /*
         stimFalse:
         VISUAL: 0 = same, 1 = different
         SEMANTIC: 0 = natural, 1 = man-made
         PHONO: 0 = pseudohomophone, 1 = control 
         */
	char respKey;
	int rxnTime;
	float accuracy;
	sf::Clock clk;
	string stimu;
	//prepare stimulus string
	textFromFile(stimu, fileStreams[trialType]);
	setText(&stimText, stimu);
	out << highLoad << "," << string(stimText.getString()) << "," << stimFalse << ",";
	t.setHighLoad(highLoad);
	//inter-trial Interval
	window.draw(fixationCross);
	window.display();
	t.setTimer(interTrialTime);
	//grid with dots;
	t.stimGrid();
	t.setTimer(displayGridTime);
	clearScreen();
	//mask grid
	t.emptyGrid();
	t.setTimer(maskGridTime);
	clearScreen();
	//fixation
	window.draw(fixationCross);
	window.display();
	t.setTimer(fixationTime);
	clearScreen();
	//reading task
	window.draw(stimText);
	window.draw(leftReminder);
	window.draw(rightReminder);
	window.display();
	clk.restart();
	respKey = 'A' + t.getResponse(&yesKey, &noKey);
	rxnTime = clk.restart().asMilliseconds();
	out << respKey << "," << rxnTime << ",";
	if (stimFalse && respKey == yesKeyChar) { out << 1;}
	else if (!stimFalse && respKey == noKeyChar) { out << 1;}
	else {out << 0;};
	out << ",";
	clearScreen();
	//memory task
	window.setMouseCursorVisible(true);
	t.respGrid();
	clk.restart();
	accuracy = t.memoryResponse(&continueText, &continueKey);
	rxnTime = clk.restart().asMilliseconds();
	out << rxnTime << "," << accuracy << ",";
	t.resetResp();
	clearScreen();
}


int main(int argc, char* argv[]){
	if (!bgTex.create(screenWidth, screenHeight)){return 1;}
	if(!font.loadFromFile("font.ttf")){return 1;}
	string outFilename = "participant";
	string participant = "";
	int participantInt = 0;
	int counterBalance;
	while(true){
		cout << "Enter participant number: ";
		if (cin >> participantInt && participantInt > 0){
			int i = participantInt;
			while (i < 100){
				participant += "0";
				i *= 10;
			}
			participant += to_string(participantInt);
			break;
		}else{
			cin.clear();
			cin.ignore();
		}
	}
	counterBalance = participantInt%2;
	for (int i = 0; i < 4; ++i){
		fileStreams[i] = counterBalance ? ifstream{fileNames[(i+2)%4]} : ifstream{fileNames[i]};
	}
	window.create(screen, "Reading", sf::Style::Fullscreen);
	srand(time(NULL));
	outFilename += participant;
	outFilename += ".txt";
	ofstream outStream{outFilename};
	window.setKeyRepeatEnabled(false); //dont generate >1 event if holding a key down
	//pick what keys mean
	int pick = rand()%2;
	yesKey = keys[pick];
	yesKeyChar = yesKey + 'A';
	pick = (pick + 1)%2;
	noKey = keys[pick];
	noKeyChar = noKey + 'A';
	//set up text displays
	instr = "Press " + string(1,yesKeyChar);
#if defined VISUAL
        instr += " if the two letters are visually the same (eg. AA).";
#elif defined SEMANTIC
        instr += " if the word refers to something natural (eg. water).";
#elif defined PHONO
        instr += " if the word SOUNDS identical to a real word (eg. BRANE).";
#endif
        instr +=  "\n Press "+ string(1,noKeyChar);
#if defined VISUAL
        instr += " if they are visually different (eg. aA).";
#elif defined SEMANTIC
        instr += " if the word refers to something man-made (eg. hammer).";
#elif defined PHONO
        instr += " if the word DOES NOT SOUND like a real word (eg. FRANE).";
#endif
        instr += "\nPress spacebar to continue.";
	leftRemind = string(1,keyChars[0]) + ": ";
	rightRemind = string(1, keyChars[1]) + ": ";
#if defined VISUAL
	leftRemind += (keyChars[0] == yesKeyChar)? "same" : "different";
	rightRemind += (keyChars[1] == yesKeyChar)? "same" : "different";
#elif defined SEMANTIC
	leftRemind += (keyChars[0] == yesKeyChar)? "natural" : "man-made";
	rightRemind += (keyChars[1] == yesKeyChar)? "natural" : "man-made";
#elif defined PHONO
	leftRemind += (keyChars[0] == yesKeyChar)? "sounds like a word" : "doesn't sound like a word";
	rightRemind += (keyChars[1] == yesKeyChar)? "sounds like a word" : "doesn't sound like a word";
#endif
	setText(&leftReminder, leftRemind);
        align(&leftReminder, false);
	setText(&rightReminder, rightRemind);
        align(&rightReminder, true);
	setText(&breakText, "Take a break\nRemember,\n" + instr);
	breakText.setPosition(screenWidth/2, screenHeight/2);
	initText(&fixationCross);
	initText(&continueText);
	initText(&endText);
	fixationCross.setPosition(screenWidth/2, screenHeight/2);
	stimText.setPosition(screenWidth/2, screenHeight/2);
	endText.setPosition(screenWidth/2, screenHeight/2);
	continueText.setPosition(screenWidth/2,gridPos.y/2);
	//make white background
	bgTex.clear(sf::Color::White);
	bgTex.display();
	bg.setTexture(bgTex.getTexture());
	//make grid objects
	HighLoad highGrid(gridSize, numDots, gridPos.x, gridPos.y);
	LowLoad lowGrid(gridSize, numDots, gridPos.x, gridPos.y);
	Empty emptyGrid(gridSize, numDots, gridPos.x, gridPos.y);
	RespMatrix resp(gridSize, numDots, gridPos.x, gridPos.y);
	Trial trialStruct(&emptyGrid, &lowGrid, &highGrid, &resp, &window, numDots);
	unsigned int trial = 1;
	unsigned int block = 0; //practice trials are block 0
	unsigned int numTrials = practiceTrials;
	clearScreen();
	//write column names of data
	outStream << "participant,counterbalance,yesKey,noKey,block,memLoad,stim,stimFalse,response,rt,incorrect,memRT,memAcc,trialNum" << endl;
	//run the experiment!
	textScreen(&introText, &trialStruct, instr);
	while (window.isOpen() && block <= numBlocks){
		while (window.isOpen() && trial <= numTrials){
			outStream << participant << "," << counterBalance << ","<< yesKeyChar << "," << noKeyChar <<"," << block <<",";
			doTrial(trialStruct, outStream);
			outStream << trial << endl;
			++trial;
		}
		inPractice = false;
		//take a break, update numTrials to loop the appropriate number of times in (trial <= numTrials) loop
		if (block < numBlocks){
                    textScreen(&breakText, &trialStruct, dummyText);
                }
		++block;
		numTrials = (block == numBlocks)? totalTrials : block*trialsPerBlock + practiceTrials;
	}
	//experiment over
	textScreen(&endText, &trialStruct, dummyText, &quitKey);
	return 0;
}
