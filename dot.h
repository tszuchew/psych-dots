#ifndef DOT_H
#define DOT_H

#include <SFML/Graphics.hpp>

struct Dot{
	sf::CircleShape dot;
	unsigned int xCell;
	unsigned int yCell;
};


#endif
