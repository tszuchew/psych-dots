#include "dotMatrix.h"
#include <cstdlib>
#include <ctime>
#include "dot.h"

DotMatrix::DotMatrix(const unsigned int s, const unsigned int n, const unsigned int xpos, const unsigned int ypos):
	size{s}, dist{s/n}, numCells{n} {
	srand(time(NULL));
	dots = new Dot[numCells];
	for (unsigned int i = 0; i < numCells; ++i){
		dots[i].dot.setRadius(size/(4*numCells));
		dots[i].dot.setFillColor(sf::Color::Black);
	}
	outline = sf::RectangleShape(sf::Vector2f(size,size));
	outline.setOutlineThickness(5);
	outline.setFillColor(sf::Color::White);
	outline.setOutlineColor(sf::Color::Black);
	setPosition(xpos,ypos);

}

DotMatrix::~DotMatrix(){
	delete [] dots;
}

void DotMatrix::setPosition(const unsigned int xp, const unsigned int yp){
	x = xp;
	y = yp;
	outline.setPosition(x, y);
	updateToNewPosition();
}

void DotMatrix::updateToNewPosition(){}

unsigned int DotMatrix::getX() const {return x;}
unsigned int DotMatrix::getY() const { return y;}
unsigned int DotMatrix::getSize() const {return size;}
sf::RectangleShape* DotMatrix::getOutline(){return &outline;}

void DotMatrix::setDotPos(const int i, const int xCell, const int yCell){
	dots[i].dot.setPosition((xCell*dist)+x+(dist/4), (yCell*dist)+y+(dist/4));
	dots[i].xCell = xCell;
	dots[i].yCell = yCell;
}

sf::Vector2i DotMatrix::getDotPos(const unsigned int i){
	return sf::Vector2i(dots[i].xCell, dots[i].yCell);
}


void DotMatrix::drawGrid(sf::RenderWindow* window) const{
	//draw outline square
	window->draw(outline);
	//draw grid lines
	sf::VertexArray tee(sf::Lines, 4);
	for (int i = 0; i < 4; ++i){
		tee[i].color = sf::Color::Black;
	}
        //grid lines are calculated each time grid is drawn, this is pretty silly
        //should really calculate once and store it
	for (unsigned int i = 1; i < numCells; ++i){
		tee[0].position = sf::Vector2f(x, y+(i*dist));
		tee[1].position = sf::Vector2f(x+size, y+(i*dist));
		tee[2].position = sf::Vector2f(x+(i*dist), y);
		tee[3].position = sf::Vector2f(x+(i*dist), y+size);
		window->draw(tee);
	}
}

void DotMatrix::drawDots(sf::RenderWindow* window){
	for(unsigned int i = 0; i < numCells; ++i){
		window->draw(dots[i].dot);
	}
}


void DotMatrix::draw(sf::RenderWindow* window){
	drawGrid(window);
	placeDots();
	drawDots(window);
}


void LowLoad::placeDots(){
	bool isVert = rand()%2;
	const int start = rand()%numCells;
	if (isVert){
		for(unsigned int i = 0; i < numCells; ++i){
			setDotPos(i, start, i);
		}
	}else{
		for (unsigned int i = 0; i < numCells; ++i){
			setDotPos(i, i, start);
		}
	}
	
}


bool HighLoad::checkNeighbours(const unsigned int i, const int xCell, const int yCell){
	for (unsigned int j = 0; j < i; ++j){
		sf::Vector2i pos = getDotPos(j);
		if ((abs(xCell - pos.x) <= 1) && (yCell == pos.y)){
		 	return true;      
		}else if((abs(yCell - pos.y) <= 1) && (xCell == pos.x)){
			return true;
		}
	}	
	return false;
}

void HighLoad::placeDots(){
	unsigned int i = 0;
	while(i < numCells){
		int y = rand()%numCells;
		int x = rand()%numCells;
		if (checkNeighbours(i, x, y)){
			continue;
		}else{
			setDotPos(i, x, y);
			++i;
		}
	}
}

void Empty::placeDots(){}
void Empty::drawDots(sf::RenderWindow* window) {}
		
